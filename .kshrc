# local .kshrc for fubar

. /etc/ksh.kshrc
PS1='$PWD $ '


# alias

alias ls="ls -A"
alias rx="ls -Al"
alias card="neofetch --size none --gpu_brand off --w3m ~/Pictures/cards/"
alias bsd="neofetch --size none --gpu_brand off --w3m ~/44bsd.gif"
alias joe="joe -nobackups"
alias pkginfo="sh ~/pkglist.sh"
alias burn="rm -rf"
alias ca="cat"
alias snap="mpv /home/fubar/music/rap/ --shuffle"
alias cpf="cp -R"
alias pdf="mupdf"
alias ltx="pdflatex"
alias q="exit"  
alias md="mkdir"
alias m="man"
alias sc="scrot -cd 5"
alias clr="clear"
alias help="cat /home/glitch/.kshrc"
alias supertux="supertux2 --datadir /usr/local/share/supertux2/"

